    $(document).ready(function(){
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    var url = $("#url_video").attr('src');
    $("#video_modal").on('hide.bs.modal', function(){
        $("#url_video").attr('src', '');
    });
    $("#video_modal").on('show.bs.modal', function(){
        $("#url_video").attr('src', url);
    });
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */

        // smoothScroll plugin scroll between sections
           smoothScroll.init({
               speed: 1500,
               easing: 'easeInOutCubic',
               offset: 0,
               updateURL: true,
               callbackBefore: function ( toggle, anchor ) {},
               callbackAfter: function ( toggle, anchor ) {}
           });
        // smoothScroll plugin scroll between sections

        /*scroll animation*/
        wow = new WOW(
        {
            boxClass:     'wow',      // default
            animateClass: 'animated', // default
            offset:       0,          // default
            mobile:       false,       // default
            live:         true        // default
        }
        )
        wow.init();
    /*scroll animation*/
});